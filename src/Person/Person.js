import React from 'react'

const person = (args) => {
    return (
        <div>
            <p>I'm a { args.name} although you think man is {Math.floor(Math.random()*60)} years... I am actually only {args.age} ! {args.children}</p>
        </div>
    )
};

export default person;