import React, { Component } from 'react';
import './App.css';
import Person from './Person/Person';

class App extends Component {
  state = {
    persons: [
      {name: 'Kassy8', age: 31},
      {name: 'Kassy2', age: 25},
      {name: 'Kassy3', age: 21},
    ]
  }

  switchNameHandler = () => {
    console.log('clickting');
    //DONT DO THIS PAL: this.state.persons[0].name= "Kasoma Eric 8";
    this.setState({
      persons: [
        {name: 'Kasoma Eric 8', age: 31},
        {name: 'Kassy2', age: 25},
        {name: 'Kassy3', age: 20},
      ]
    })
  }

  render() {
    return (
      <div className="App" id="the-id">
        <h1 className="Kassy-First-H1-Tag">I am the React Kassy</h1>
        <p className="Kassy-First-p-Tag">I am also React Kassy</p>
        <button onClick={this.switchNameHandler}>Switch Up Name</button>
        <Person name={this.state.persons[0].name} age={this.state.persons[0].age} />
        <Person name={this.state.persons[1].name} age={this.state.persons[1].age} />
        <Person name={this.state.persons[2].name} age={this.state.persons[2].age} />

      </div>
    );
        /*<Person name="Kassy" age="31" />
        <Person name="TeO" age="29">Play for Everton these days</Person>*/
    /*return React.createElement(
      'div',{className: 'App', id:'the-id'},
      React.createElement('h1',{className: 'Kassy-First-H1-Tag'},'I am the React Kassy'),
      React.createElement('p',{className: 'Kassy-First-p-Tag'},'I am also React Kassy'),
      //? React.createElement('Person',{})
    );*/
  }
}

export default App;
